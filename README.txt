
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation

INTRODUCTION
------------

Current Maintainer: Bastlynn <bastlynn@gmail.com>

You may be familiar with Comic-press, the Wordpress theme for webcomics. Comicdrop is a similar effort - we want to get you up and running with a webcomic as quickly as possible with as little fuss as about it as we can. Comicdrop keeps your comic features separated from your theme so updates to your Drupal installation or your theme won't damage comicdrop.

Drupal includes a forum as well as a comments system, which use the same set of accounts, so you won't need to go to another site to host your community forum and you won't spend effort building multiple communities around your comic.

Comicdrop's out of the box features include: community forums, a commenting system, calendar, search system, and the ability to add accounts to specific roles so if you have multiple writers or artists, you can give their accounts additional power that a regular user account doesn't normally get without giving them the keys to the kingdom.

You have a lot of room to grow on a Comicdrop install. This configuration file is just the surface of what you can do with a Drupal install. The suggestions file has a tutorial on how to create a page to allow your audience to send you a commission request, as well as a few other tutorials and suggestions.

And of course, you have the power of the Drupal community at your back. Drupal is an open source project in use by many large enterprise businesses and government departments. We're not going away anytime soon. Drupal.org is stuffed to the gills with developers, themers, and advice givers. We have thousands of modules contributed by developers and webshops around the world which are constantly being improved upon.

Take a moment to visit the SUGGESTIONS.html file for more suggestions and tutorials, including configuring human (and SEO) friendly URLs, XML Sitemaps for search engines, and how to set up a store for when you really hit it big.

INSTALLATION
------------

1. Copy this comicdrop/ directory to your sites/SITENAME/modules directory. 

2. Enable the module at  http://www.example.com?q=admin/modules

3. Configure comicdrop settings (and initialize a basic install configuration) at http://www.example.com?q=admin/structure/comicdrop